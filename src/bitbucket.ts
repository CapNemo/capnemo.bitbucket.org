/// <reference path="../typings/angularjs/angular.d.ts"/>
/// <reference path="../typings/jquery/jquery.d.ts"/>
/**
 * @author Maximilian Weiler
 * This Module can be used to query and represent data served by the Bitbucket-REST-API
 * in Typescript.
 * see http://restbrowser.bitbucket.org/
 */
module Bitbucket {
  export namespace Queries {
    var API_URL = "https://bitbucket.org/api/2.0/";
    var GET_USERS_REPOSITORIES = API_URL + "repositories/";
    var GET_USER_INFO = API_URL + "'users/";


    export function getUsersRepositories(username: string): string {
      return GET_USERS_REPOSITORIES + username + "/";
    }

    export function getUserInfo(username: string): string {
      return GET_USER_INFO + username + "/";
    }

    export function getRepositoryInfo(username: string, reposlug: string) {
      getUsersRepositories(username) + reposlug + "/";
    }

    export function getCommits(username: string, reposlug: string, revision: string) {
      return getRepositoryInfo(username, reposlug) + "commits/" + revision + "/";
    }
  }

  export interface Link {
    href: string;
  }

  export interface Links {
    self: Link,
    html: Link,
    avatar: Link
  }

  export interface IUser {
    username: string;
    website: string;
    display_name: string;
    uuid: string;
    links: UserLinks;
    created_on: string;
    location: string;
    type: string;

  }

  export interface UserLinks extends Links {
    hooks: Link;
    repositories: Link;
    followers: Link;
    following: Link;
    snippets: Link;
  }

  export interface RepositoriesInfo {
    pagelen: number;
    values: IRepository[];
  }

  export interface IRepository {
    scm: string,
    has_wiki: boolean;
    name: string;
    links: RepoLinks;
    fork_policy: string,
    uuid: string;
    language: string;
    created_on: string;
    full_name: string;
    has_issues: boolean;
    owner: IUser;
    updated_on: string;
    size: number;
    type: string;
    is_private: boolean;
    description: string;
  }

  export interface RepoLinks extends Bitbucket.Links {
    hooks: Link;
    watchers: Link;
    clone: CloneLink[];
    commits: Link;
    forks: Link;
    downloads: Link;
    pullrequests: Link;
  }

  export interface CloneLink extends Link {
    name: string;
  }
}