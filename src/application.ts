/// <reference path="../typings/angularjs/angular.d.ts"/>
/// <reference path="../typings/jquery/jquery.d.ts"/>
/// <reference path="../typings/angular-ui-bootstrap/angular-ui-bootstrap.d.ts"/>
/// <reference path="bitbucket.ts"/>

module ProjectPage.Config {
  export interface IConfig {
    personalization: PersonalizationConfig,
    general: GeneralConfig
  }

  export interface PersonalizationConfig {
    username: string,
    displayName: string
  }

  export interface GeneralConfig {
    enableFetchFromBitbucket: boolean,
    enableBlog: boolean,
    enableLinkGeneration: boolean,
    defaultTheme: string,
    enableThemeChange: boolean,
    availableThemes: string[];
  }
}


/**
 * Module holding the Scope, Controller and Models used in this application
 */
module ProjectPage {
  var CONFIG_PATH = "config.json";
  var PROJECTS_PATH = "projects.json";
  var DEFAULT_THEME = "bootstrap";
  var START_URL = "start.html";
  var DEFAULT_HOSTNAME = "bitbucket.org";
	
	
	/**
	 * Main scope layout
	 */
  interface ProjectScope extends ng.IScope {
    config: Config.IConfig;
    projects: Model.IProjectModel[];
    url: string;
    css: string;
    alerts: IAlert[];
    closeAlert: (id: number) => void;
    openImgModal: (url: string, descripton: string) => void;
    changeStyle: (style: string) => void;
    gotoPage: (url: string) => void;
  }

  interface IAlert {
    type: string;
    msg: string;
  }
	
	
	/**
	 * Angular-Controller of this application
	 */
  export class ProjectController {
    private repositoryInfo: Bitbucket.RepositoriesInfo;
    private config: Config.IConfig;
    private scope: ProjectScope;
    private http: ng.IHttpService;
    private modalService: ng.ui.bootstrap.IModalService;
		
		/**
		 * Constructor
		 */
    constructor($scope: ProjectScope, $http: ng.IHttpService, private $uibModal: ng.ui.bootstrap.IModalService) {
      //Initialize $scope empty
      this.scope = $scope;
      this.http = $http;
      this.modalService = $uibModal;
      this.initDefaultScopeData();
      this.loadConfig(() => {
        this.loadProjects(() => {
          this.loadFromApi(this.networkErrorHandler.bind(this));
        }, this.networkErrorHandler.bind(this));
      });
    }

		/**
		 * Loads the config.json file and updates $scope.config if found
		 */
    private loadConfig(successCallback: () => void, errorCallback?: () => void) {
      var configRequest = this.http.get(CONFIG_PATH);
      configRequest.success((loadedConfig: Config.IConfig) => {
        this.config = loadedConfig;
        this.scope.config = loadedConfig;
        if (loadedConfig.general.defaultTheme) {
          this.scope.css = loadedConfig.general.defaultTheme;
        }
        successCallback();
      });
      if (errorCallback) {
        configRequest.error(errorCallback);
      }
    }

		/**
		 * Loads the projects.json file and updates $scope.projects if found
		 */
    private loadProjects(successCallback: () => void, errorCallback?: () => void) {
      var projectsRequest = this.http.get(PROJECTS_PATH);
      projectsRequest.success((projects: Array<Model.IProjectModel>) => {
        this.scope.projects = projects;
        if (this.config.general.enableLinkGeneration) {
          projects.forEach(project => {
            this.generateDownloadAndRepoLink(project);
          });
        }
        successCallback();
      });
      if (errorCallback) {
        projectsRequest.error(errorCallback);
      }
    }
		
		/**
		 * Generates Download- and Repository-Link matching the current username and the project-title.
		 * Requires that the project-title matches the repository-name
		 */
    private generateDownloadAndRepoLink(project: Model.IProjectModel) {
      var username = this.config.personalization.username;
      //Set download-link form Repository if not already specified in projects.json
      project.download = project.download || 'https://bitbucket.org/' + username + "/" + project.title + '/get/HEAD.zip';
      //Set description form Repository if not already specified in projects.json
      project.repo = project.repo || "https://bitbucket.org/" + username + "/" + project.title;
    }

		/**
		 * Loads additional Information from the Bitbucket-API if enabled in configuration
		 * and updates $scope.projects accordingly
		 */
    private loadFromApi(errorCallback?: () => void) {
      if (this.scope.config.general.enableFetchFromBitbucket && this.isOnTargetHost()) {
        var api_request = this.http.get(Bitbucket.Queries.getUsersRepositories(this.scope.config.personalization.username));
        api_request.success((repoInfo: Bitbucket.RepositoriesInfo) => {
          this.appendRepoInfo(repoInfo);
        });
        if (errorCallback) {
          api_request.error(errorCallback);
        }
      }
    }
		
		
		/**
		 * Initializes the Scope
		 */
    private initDefaultScopeData() {
      this.scope.alerts = [];
      this.scope.projects = [];
      this.scope.url = START_URL;
      this.scope.css = DEFAULT_THEME;
      this.scope.closeAlert = this.closeAlert.bind(this);
      this.scope.openImgModal = this.openImgModal.bind(this);
      this.scope.changeStyle = this.setStyle.bind(this);
      this.scope.gotoPage = this.gotoPage.bind(this);
    }

		/**
		 * Test if the page is served on the right host (hostname).
		 * Always false if page is opened via file: protocol
		 */
    private isOnTargetHost() {
      return window.location.host === this.config.personalization.username.toLowerCase() + "." + DEFAULT_HOSTNAME;
    }

		/**
		 * Removes the alert with index
		 */
    private closeAlert(index: number) {
      this.scope.alerts.splice(index, 1);
    }

		/**
		 * Handler function for failed $http.get - requests
		 */
    private networkErrorHandler = (result: any) => {
      this.scope.alerts.push({ type: 'danger', msg: 'Netzwerkfehler: Einige Informationen können aktuell nicht angezeigt werden' });
    }
		
		/**
		 * Change the current stylesheed to one contained in /assets/css/{{name}}.min.css
		 */
    private setStyle(name: string) {
      this.scope.css = name;
    }
		
		/**
		 * Switch to a subpage specified by url
		 */
    private gotoPage(url: string) {
      this.scope.url = url;
    }

		/**
		 * Displays an Modal with an image and a description if one exists
		 */
    private openImgModal(url: string, description?: string) {
      var modalSettings: ng.ui.bootstrap.IModalSettings = <ng.ui.bootstrap.IModalSettings>{
        animation: true,
        templateUrl: "assets/templates/imgmodal.html",
        controller: "ImgModalCtrl",
        size: "lg",
        resolve: {
          url: () => { return url },
          description: () => { return description }
        }
      };
      var modalInstance = this.$uibModal.open(modalSettings);
      modalInstance.result.then(() => {
        //Do something when the modal is closed
      })
    }

		/**
		 * Appends useful Information from the Bitbucket-API to $scope.projects.
		 * This extends projects.json whenever possible. 
		 */
    private appendRepoInfo(data: Bitbucket.RepositoriesInfo) {
      var projects = <Model.ProjectModel[]>this.scope.projects;
      var nameIdMap: { [s: string]: number } = {};
      for (var i = 0; i < projects.length; ++i) {
        var current: Model.ProjectModel = projects[i];
        nameIdMap[current.title] = i;
      }
      data.values.forEach(repo => {
        var index: number = nameIdMap[repo.name];
        var alreadyExists: boolean = (index != null && index != undefined);
        if (alreadyExists) {
          var current = projects[index];
        } else {
          var current = new ProjectPage.Model.ProjectModel({ "title": repo.name });
          if (this.config.general.enableLinkGeneration) {
            this.generateDownloadAndRepoLink(current);
          }
        }
        //Set description form Repository if not already specified in projects.json
        current.description = current.description || repo.description;
        //Set the language form Repository if not already specified in projects.json
        current.languages = current.languages || [repo.language];
        //repo does not exist in projects.json
        if (!alreadyExists) {
          projects.push(new Model.ProjectModel(current));
        }
      });
    }
  }    
	
	/**
	 * Interface that defines all currently needed data in l20n.js
	 */
  interface CustomL20nData {
    count: number;
  }
}


/**
 * Module defining UI-Elements or Controllers used in this application
 */
module ProjectPage.UI {
	
	/**
	 * Scope used in ImgModalCtrl
	 */
  interface ImgModalScope extends ng.IScope {
    url: string,
    description: string;
    close: Function;
  }

	/**
	 * Controller for a Modal instance.
	 * Provides url and description of the image and a function  to close the modal.
	 */
  export class ImgModalCtrl {
    constructor(private $scope: ImgModalScope, private $uibModalInstance: ng.ui.bootstrap.IModalServiceInstance, url: string, description: string) {
      $scope.url = url;
      $scope.description = description;
      $scope.close = () => {
        $uibModalInstance.close();
      };
    }
  }
}

/**
 * Module holding data about projects
 */
module ProjectPage.Model {
  export interface IProjectModel {
    title: string;
    description?: string;
    languages?: string[];
    repo?: string;
    website?: string;
    download?: string;
    screenshots?: Array<UrlDescriptionPair>;
  }

	/**
	 * Interface for some Object with url and description, just like figures.
	 */
  interface UrlDescriptionPair {
    url: string;
    description: string;
  }
	
	/**
	 * Model representing one project with title, description and url
	 */
  export class ProjectModel implements IProjectModel {
    //Attributes
    public title: string;
    public description: string;
    public repo: string;
    public languages: string[];
    public website: string;
    public download: string;
    public screenshots: Array<UrlDescriptionPair>;
		
		/**
		 * Copy-Constructor
		 */
    constructor(project: IProjectModel) {
      this.title = project.title;
      this.description = project.description;
      this.repo = project.repo;
      this.languages = project.languages;
      this.website = project.website;
      this.download = project.download;
      this.screenshots = project.screenshots;
    }
  }
}    

/* ----- Initialize Angular Module and Controller */
var project_page = angular.module('ProjectPage', ["ui.bootstrap", "ngL20n"]);
project_page.controller('ProjectPage.ProjectController', ProjectPage.ProjectController);
project_page.controller('ImgModalCtrl', ProjectPage.UI.ImgModalCtrl);