var gulp = require('gulp');
var ts = require('gulp-typescript');
var merge = require('merge2');
var sass = require('gulp-sass');
var typedoc = require("gulp-typedoc");
var tsProject = ts.createProject('src/tsconfig.json');

gulp.task('scripts', function() {
  var tsResult = tsProject.src('*.ts')
    .pipe(ts(tsProject));
  return merge([
    tsResult.dts.pipe(gulp.dest('assets/definitions')),
    tsResult.js.pipe(gulp.dest('assets/js'))
    ]);
});

gulp.task('sass', function () {
  gulp.src('*.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
});
 
gulp.task('watch', function () {
  gulp.watch('*.scss', ['sass']);
  gulp.watch('src/*.ts', ['scripts']);
});

gulp.task("typedoc", function() {
    return gulp.src('*.ts')
        .pipe(typedoc({
            module: 'amd',
            target: 'es5',
            out: 'docs/',
            name: 'CapNemo Projectpage'
        }))
    ;
});