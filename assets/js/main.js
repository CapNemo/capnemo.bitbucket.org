var Bitbucket;
(function (Bitbucket) {
    var Queries;
    (function (Queries) {
        var API_URL = "https://bitbucket.org/api/2.0/";
        var GET_USERS_REPOSITORIES = API_URL + "repositories/";
        var GET_USER_INFO = API_URL + "'users/";
        function getUsersRepositories(username) {
            return GET_USERS_REPOSITORIES + username + "/";
        }
        Queries.getUsersRepositories = getUsersRepositories;
        function getUserInfo(username) {
            return GET_USER_INFO + username + "/";
        }
        Queries.getUserInfo = getUserInfo;
        function getRepositoryInfo(username, reposlug) {
            getUsersRepositories(username) + reposlug + "/";
        }
        Queries.getRepositoryInfo = getRepositoryInfo;
        function getCommits(username, reposlug, revision) {
            return getRepositoryInfo(username, reposlug) + "commits/" + revision + "/";
        }
        Queries.getCommits = getCommits;
    })(Queries = Bitbucket.Queries || (Bitbucket.Queries = {}));
})(Bitbucket || (Bitbucket = {}));
var ProjectPage;
(function (ProjectPage) {
    var CONFIG_PATH = "config.json";
    var PROJECTS_PATH = "projects.json";
    var DEFAULT_THEME = "bootstrap";
    var START_URL = "start.html";
    var DEFAULT_HOSTNAME = "bitbucket.org";
    var ProjectController = (function () {
        function ProjectController($scope, $http, $uibModal) {
            var _this = this;
            this.$uibModal = $uibModal;
            this.networkErrorHandler = function (result) {
                _this.scope.alerts.push({ type: 'danger', msg: 'Netzwerkfehler: Einige Informationen können aktuell nicht angezeigt werden' });
            };
            this.scope = $scope;
            this.http = $http;
            this.modalService = $uibModal;
            this.initDefaultScopeData();
            this.loadConfig(function () {
                _this.loadProjects(function () {
                    _this.loadFromApi(_this.networkErrorHandler.bind(_this));
                }, _this.networkErrorHandler.bind(_this));
            });
        }
        ProjectController.prototype.loadConfig = function (successCallback, errorCallback) {
            var _this = this;
            var configRequest = this.http.get(CONFIG_PATH);
            configRequest.success(function (loadedConfig) {
                _this.config = loadedConfig;
                _this.scope.config = loadedConfig;
                if (loadedConfig.general.defaultTheme) {
                    _this.scope.css = loadedConfig.general.defaultTheme;
                }
                successCallback();
            });
            if (errorCallback) {
                configRequest.error(errorCallback);
            }
        };
        ProjectController.prototype.loadProjects = function (successCallback, errorCallback) {
            var _this = this;
            var projectsRequest = this.http.get(PROJECTS_PATH);
            projectsRequest.success(function (projects) {
                _this.scope.projects = projects;
                if (_this.config.general.enableLinkGeneration) {
                    projects.forEach(function (project) {
                        _this.generateDownloadAndRepoLink(project);
                    });
                }
                successCallback();
            });
            if (errorCallback) {
                projectsRequest.error(errorCallback);
            }
        };
        ProjectController.prototype.generateDownloadAndRepoLink = function (project) {
            var username = this.config.personalization.username;
            project.download = project.download || 'https://bitbucket.org/' + username + "/" + project.title + '/get/HEAD.zip';
            project.repo = project.repo || "https://bitbucket.org/" + username + "/" + project.title;
        };
        ProjectController.prototype.loadFromApi = function (errorCallback) {
            var _this = this;
            if (this.scope.config.general.enableFetchFromBitbucket && this.isOnTargetHost()) {
                var api_request = this.http.get(Bitbucket.Queries.getUsersRepositories(this.scope.config.personalization.username));
                api_request.success(function (repoInfo) {
                    _this.appendRepoInfo(repoInfo);
                });
                if (errorCallback) {
                    api_request.error(errorCallback);
                }
            }
        };
        ProjectController.prototype.initDefaultScopeData = function () {
            this.scope.alerts = [];
            this.scope.projects = [];
            this.scope.url = START_URL;
            this.scope.css = DEFAULT_THEME;
            this.scope.closeAlert = this.closeAlert.bind(this);
            this.scope.openImgModal = this.openImgModal.bind(this);
            this.scope.changeStyle = this.setStyle.bind(this);
            this.scope.gotoPage = this.gotoPage.bind(this);
        };
        ProjectController.prototype.isOnTargetHost = function () {
            return window.location.host === this.config.personalization.username.toLowerCase() + "." + DEFAULT_HOSTNAME;
        };
        ProjectController.prototype.closeAlert = function (index) {
            this.scope.alerts.splice(index, 1);
        };
        ProjectController.prototype.setStyle = function (name) {
            this.scope.css = name;
        };
        ProjectController.prototype.gotoPage = function (url) {
            this.scope.url = url;
        };
        ProjectController.prototype.openImgModal = function (url, description) {
            var modalSettings = {
                animation: true,
                templateUrl: "assets/templates/imgmodal.html",
                controller: "ImgModalCtrl",
                size: "lg",
                resolve: {
                    url: function () { return url; },
                    description: function () { return description; }
                }
            };
            var modalInstance = this.$uibModal.open(modalSettings);
            modalInstance.result.then(function () {
            });
        };
        ProjectController.prototype.appendRepoInfo = function (data) {
            var _this = this;
            var projects = this.scope.projects;
            var nameIdMap = {};
            for (var i = 0; i < projects.length; ++i) {
                var current = projects[i];
                nameIdMap[current.title] = i;
            }
            data.values.forEach(function (repo) {
                var index = nameIdMap[repo.name];
                var alreadyExists = (index != null && index != undefined);
                if (alreadyExists) {
                    var current = projects[index];
                }
                else {
                    var current = new ProjectPage.Model.ProjectModel({ "title": repo.name });
                    if (_this.config.general.enableLinkGeneration) {
                        _this.generateDownloadAndRepoLink(current);
                    }
                }
                current.description = current.description || repo.description;
                current.languages = current.languages || [repo.language];
                if (!alreadyExists) {
                    projects.push(new ProjectPage.Model.ProjectModel(current));
                }
            });
        };
        return ProjectController;
    })();
    ProjectPage.ProjectController = ProjectController;
})(ProjectPage || (ProjectPage = {}));
var ProjectPage;
(function (ProjectPage) {
    var UI;
    (function (UI) {
        var ImgModalCtrl = (function () {
            function ImgModalCtrl($scope, $uibModalInstance, url, description) {
                this.$scope = $scope;
                this.$uibModalInstance = $uibModalInstance;
                $scope.url = url;
                $scope.description = description;
                $scope.close = function () {
                    $uibModalInstance.close();
                };
            }
            return ImgModalCtrl;
        })();
        UI.ImgModalCtrl = ImgModalCtrl;
    })(UI = ProjectPage.UI || (ProjectPage.UI = {}));
})(ProjectPage || (ProjectPage = {}));
var ProjectPage;
(function (ProjectPage) {
    var Model;
    (function (Model) {
        var ProjectModel = (function () {
            function ProjectModel(project) {
                this.title = project.title;
                this.description = project.description;
                this.repo = project.repo;
                this.languages = project.languages;
                this.website = project.website;
                this.download = project.download;
                this.screenshots = project.screenshots;
            }
            return ProjectModel;
        })();
        Model.ProjectModel = ProjectModel;
    })(Model = ProjectPage.Model || (ProjectPage.Model = {}));
})(ProjectPage || (ProjectPage = {}));
var project_page = angular.module('ProjectPage', ["ui.bootstrap", "ngL20n"]);
project_page.controller('ProjectPage.ProjectController', ProjectPage.ProjectController);
project_page.controller('ImgModalCtrl', ProjectPage.UI.ImgModalCtrl);
